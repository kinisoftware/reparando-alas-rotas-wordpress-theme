      <hr>
      
      <footer> 
        <div class="row-fluid">
          <div class="span6">       
            <p>&copy; Reparando Alas Rotas desde 2013</p>
          </div>
          <div class="span4 offset2">
            <div class="social">
              <li> <a href="https://www.facebook.com/pages/Reparando-Alas-Rotas/507914229227065"><img src="<?php bloginfo('template_directory'); ?>/img/facebook.png"/></a>
              </li>
              <li><a href="http://twitter.com/ReparandoAlas"><img src="<?php bloginfo('template_directory'); ?>/img/twitter.png"/></a>
              </li>              
              <li><a href="mailto:hola@reparandoalasrotas.com"><img src="<?php bloginfo('template_directory'); ?>/img/mail_footer.png"/></a>
              </li>              
            </div>
          </div>
        </div>
      </footer>

    </div> <!-- /container -->

    <?php wp_footer(); ?>
  </body>
  </html>
